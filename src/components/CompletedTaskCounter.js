import React from 'react';
import "../styles/CompletedTaskCounter.css"

function CompletedTaskCounter({ count }) {
    return (
        <div
        style={{
          backgroundColor: '#72079E',
          borderRadius: 80,
          height: 30,
          width: 250,
          textAlign: 'center',
          marginTop: 5,
          marginRight: '30px'
        }}
      >

            <p style={{
              color: '#fff',
              fontSize: 15,
              fontWeight: 'bold',
              marginTop: 5,
            }}
          >
            Tareas completadas: {count}</p>
        </div>
    );
}

export default CompletedTaskCounter;
