import React from 'react';
import "../styles/PendingTaskCounter.css"

function PendingTaskCounter({ count }) {
    return (
        <div
          style={{
            backgroundColor: '#72079E',
            borderRadius: 80,
            height: 30,
            width: 250,
            textAlign: 'center',
            marginTop: 5,
            marginRight: '30px',
          }}
        >

            <p
            style={{
              color: '#fff',
              fontSize: 15,
              fontWeight: 'bold',
              marginTop: 5,
            }}
          >
            Tareas pendientes: {count}</p>
        </div>
    );
}

export default PendingTaskCounter;
